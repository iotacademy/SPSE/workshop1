# Vítej na prvním IoT workshopu pro školu SPŠE Plzeň

V tomto repozitáři najdeš základní projekty, na kterých si můžeš vyzkoušet jak SPSE-WS01-TEMP deska pracuje, jak ji ovládat, nastavit a jak pomoci ní odesílat data efektivně na LoRa GateWay.

Deska SPSE-WS01-TEMP je vybavena SHTC3 senzorem od firmy Sensirion, který komunikuje skrze I2C sběrnici, dále je na desce k nalezení Fuel Gauge, který dokáže měřit kapacitu baterie a také komunikuje skrze I2C sběrnici a samozřejmě je na desce osazený M2 slot pro komunikační kartu, kterou je v tomto případě MuRata SyChip ABZ. Jde o komunikační kartu ovládanou skrze sběrnici UART pomocí tzv. AT přikazů. Všechny informace o senzorech a komunikační kartě naleznete ve složce [Docs](Docs/).

Tento repozitář obsahuje jak již bylo zmíněno plno příkladů projektů, které si níže popíšeme.

## Příklady

Aby bylo co nejednodušší pochopit jak dané věci na desce fungují a jak je ovládat, tak je zde vytvořeno pár příkladů, které by měli objasnit chování jednotlivých periferií desky, potažmo samotného mikrokontroleru.

Každy příklad je označen jménem, který je specifický pro oblas, kterou popisuje. Důležité zde ale je GS a GN označení jednotlivých příkladů. **GS** znamená **GENERATED AS SECURE**, to znamená, že v tomto případě používáme main ze secure části projektu. **GN** oproti tomu znamená **GENERATED AS NONSECURE**, to znamená, že v tomto případě používáme main z nonsecure části projektu. Toto rozdělení bylo nutné pro správnou orientaci v projektech. Rozdělení do secure a nonsecure části je specifické pro mikrokontroler osazený na desce SPSE-WS01-TEMP, kde se nachází mikrokontroler SAML11E16A. Pro více informací hledejte [zde](https://ww1.microchip.com/downloads/en/DeviceDoc/SAM-L10L11-Family-DataSheet-DS60001513F.pdf).

##

### 1) LED Blinking GS

Tento projekt je nejjednodušší, jde pouze o ovládání GPIO pinu na desce SPSE-WS01-TEMP. V našem případě můžeme ovládat USER_LED, která se na desce nachází. V projektu se také používá delay funkce, kterou vytváříme z fukce **SYSTICK_DelayMs()**. LED diodu pak ovládáme z předgenerovaných maker **LED_Set()**, **LED_Clear()**, **LED_Toggle()**.

Odkaz na příklad je [zde](1\)Led_blinking_GS/).

##

### 2) USART Test GS

Jde o jednoduché využití základní USART komunikace skrze **Serial console** header. Ovládání samotné komunikace probíhá velice jednoduše v základu nám stačí pouze jedna funkce **SERCOM2_USART_Write()**, kterou jsme schopni zapsat data na **Serial console** header.

Odkaz na příklad je [zde](2\)USART_test_GS/).

##

### 3) USART stdio debug GS

Jde o jednodušší projekt, který opět pracuje s USART komunikací a posílá data na **Serial console** header. Komunikace je zde ale ulehčena za použití funkcí z stdio.h knihovny. Pro odesílání dat na USART sběrnici nám teď stačí pouze jedna funkce printf(), která je velmi jednoduchá na používání.

Odkaz na příklad je [zde](3\)USART_stdio_debug_GS/).

##

### 4) USART Modul test GS

V tomto příkladu je vidět jak odesílat data do M2 slotu do komunikační karty (MuRata SyChip ABZ) pomoci USART komunikace. Používají se zde dvě funkce **SERCOM1_USART_Write()** a **SERCOM1_USART_Read()**. V tomto příkladu odesíláme AT příkazy do LoRa modulu a čteme co nám modul odpovídá.

Odkaz na příklad je [zde](4\)USART_modul_test_GS/).

##

### 5) USART Modul basic GS

Jde o rozšíření příkladu číslo 4. Je zde poukázáno na dva různé typy připojení do sítě LoRa. (ABP, OTAA). Opět se zde používají dvě funkce **SERCOM1_USART_Write()** a **SERCOM1_USART_Read()**. Opět se odesílají AT příkazy do LoRa modulu a čte se co modul odpovídá.

Odkaz na příklad je [zde](5\)USART_modul_basic_GS/).

##

### 6) Calendar periodic actions GS

Příklad vytvoření projektu, kde se periodicky po nějakém čase vykonávají úkoly bez použití funkce delay. Jde o využití interního RTC čítače ve funkci kalendáře. Na počátku se musí RTC jasně určit jaký je čas a poté je možné nastavit tzv. alarmy, které se vykonávají po určitém čase. 

Odkaz na příklad je [zde](6\)Calendar_periodic_actions_GS/).

##

### 7) Timer harmony core GN

Příklad vytvoření projektu, kde se opět periodicky po nějakém čase vykonávají úkoly bez použití funkce delay. Jde o využití interního TC čítače. Na počátku se nemusí timeru nastavovat žádný čas, pouze se tzv. spustí a zadá se mu za jak dlouho dobu má něco vykonat. 

Odkaz na příklad je [zde](7\)Timer_harmony_core_GN/).

##

### 8) Timer harmony core GS

Ůplně stejný příklad, akorát vygenerovaný jako secure.

Odkaz na příklad je [zde](8\)Timer_harmony_core_GS/).

##

### 9) WhatTimeIsIt GS

Jde o příklad využití interního RTC, kde se na začátku předá RTC přesný čas kdy došlo k kompilaci projektu na základě preprocesorových maker **\_\_TIME\_\_** a **\_\_DATE\_\_**. Program dál díky tomuto nastavení ví za pomoci RTC jaký je aktuální čas a dokáže vypisovat aktuální čas do terminálu.

Odkaz na příklad je [zde](9\)WhatTimeIsIt_GS/).

##

### 10) LowPowerModes GN

V tomto příkladu si ukážeme low power mód a také jak se do tohoto módů dostat.

Odkaz na příklad je [zde](10\)LowPowerModes_GN/).

##

### 11) I2C Search GS

V tomto příkladu je vidět, jak probíhá práce s I2C sběrnicí. Používáme zde pouze dvě funkce **SERCOM0_I2C_Read()** a **SERCOM0_I2C_Write()**. Tento příklad slouží také jako hledání připojených zařízení na I2C sběrnici. Pokud není senzor vidět v tomto hledání, dost pravděpodobně nefunguje správně, nebo není vůbec připojen.

Odkaz na příklad je [zde](11\)I2C_Search_GS/).

##

## Projekt: SPSE-WS01-TEMP-rev.1.0 GS

Projekt SPSE-WS01-TEMP-rev.1.0 je již komplexní projekt postavený na předchozích příkladech. Používá RTC jako kalendář pro ponětí o čase, dále používá USART sběrnici pro **M2 slot** a také pro **Serial console** a v poslední řadě využívá také **I2C sběrnice** pro komunikaci se senzorem SHTC3. Fuel Gauge zde není implementovaný, ten čeká na tebe, až ho přidáš do programu a budeš i z tohoto senzoru vyčítat data o kapacitě baterie.

Odkaz na projekt je [zde](SPSE-WS01-TEMP-rev.1.0/).
