#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
#include <string.h>

char *pole_retezcu[6] = { // Pole ukazatelu (adres) na kterych jsou ulozeny jednotlive retezce zakoncenen prislusnym koncovym znakem \0
    "Komunikace ",
    "bezi ",
    "v ",
    "poradku.",
    "\r\n",
    0
};

char test_retezec[] = "Test 2 ;-)\r\n";


uint8_t my_usart_TX_call_happend = 0; // Vlajka pokud doslo ke spravnemu odeslani dat skrze USART
uint8_t my_usart_RX_call_happend = 0; // Vlajka pokud doslo ke spravnemu prijmuti dat skrze USART


void my_sercom2_usart_write_callback( uintptr_t context ){
    my_usart_TX_call_happend = 1;
}

void my_sercom2_usart_read_callback( uintptr_t context ){
    my_usart_RX_call_happend = 1;
}


int secure_main( void ){
    
    SYSTICK_TimerStart(); // Zapnuti casovace pro funkci delay
    
    /********* Nastaveni seriove komunikace RX and TX callback *********/
    SERCOM2_USART_WriteCallbackRegister(my_sercom2_usart_write_callback, 0);
    SERCOM2_USART_ReadCallbackRegister(my_sercom2_usart_read_callback, 0);
    /*******************************************************************/
    
    
    /******************* Nastaveni seriove komunikace ******************/
    USART_SERIAL_SETUP my_sercom2_usart_setting;

    my_sercom2_usart_setting.baudRate = 115200;
    my_sercom2_usart_setting.dataWidth = USART_DATA_8_BIT;
    my_sercom2_usart_setting.parity = USART_PARITY_NONE;
    my_sercom2_usart_setting.stopBits = USART_STOP_1_BIT;
    
    SERCOM2_USART_SerialSetup(&my_sercom2_usart_setting, 0);
    /*******************************************************************/

    
    SERCOM2_USART_Write((void *)"TEST\r\n", 6); // Odeslani zpravy skrze USART komunikaci
    LED_Toggle(); // Preklapeni stavu LED diody
    SYSTICK_DelayMs(1000); // Casove spozdeni 1s
    
    while ( true )
    {
        for(uint8_t i = 0; i < 5; i++){
            while(SERCOM2_USART_WriteIsBusy()); // Cekani dokud je vsechna predchozi komunikace odesilani dokoncena
            SERCOM2_USART_Write((void *)pole_retezcu[i], strlen(pole_retezcu[i])); // Postupne vypisovani pole
        }
        
        SYSTICK_DelayMs(1000); // Casove spozdeni 1s
        
        while(SERCOM2_USART_WriteIsBusy()); // Cekani dokud je vsechna predchozi komunikace odesilani dokoncena
            
        SERCOM2_USART_Write((void *)test_retezec, strlen(test_retezec)); // Vypsani textu do terminalu z promenne test_retezec
        
        SYSTICK_DelayMs(1000); // Casove spozdeni 1s
    }
}