#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
#include <time.h>

volatile uint8_t _alarm_happend = 0;

void RTC_Callback(RTC_CLOCK_INT_MASK int_cause , uintptr_t  context)
{
    if (int_cause & RTC_CLOCK_INT_MASK_ALARM0)
    {
        _alarm_happend = 1;
    }
}


int secure_main ( void ){
    
    /* Initialize System Time and Alarm Time */
    struct tm sys_time;
    struct tm alarm_time;

    /* Register Callback */
    RTC_RTCCCallbackRegister(RTC_Callback, (uintptr_t) NULL);


    /* Set Time and date
      15-01-2018 12:00:00 Monday */
    sys_time.tm_hour = 12;      /* hour [0,23] */
    sys_time.tm_sec = 00;       /* seconds [0,61] */
    sys_time.tm_min = 00;       /* minutes [0,59] */
    sys_time.tm_mon = 0;        /* month of year [0,11] */
    sys_time.tm_year = 118;     /* years since 1900 */
    sys_time.tm_mday = 15;      /* day of month [1,31] */
    sys_time.tm_wday = 1;       /* day of week [0,6] (Sunday = 0) */
                                /* tm_yday - day of year [0,365] */
                                /* tm_isdst - daylight savings flag */

    RTC_RTCCTimeSet(&sys_time);


    /* Set Alarm Time and date. Generate alarm every day when Hour, Minute and Seconds match.
       15-01-2018 12:00:20 Monday */
    alarm_time.tm_hour = 12;
    alarm_time.tm_sec = 3;
    alarm_time.tm_min = 00;
    alarm_time.tm_mon = 0;
    alarm_time.tm_year = 118;
    alarm_time.tm_mday = 15;
    alarm_time.tm_wday = 1;

    RTC_RTCCAlarmSet(&alarm_time, RTC_ALARM_MASK_SS);
    
    
    while ( 1 )
    {
        if(_alarm_happend == 1)
        {
            LED_Toggle();
            _alarm_happend = 0;
        }
    }
}