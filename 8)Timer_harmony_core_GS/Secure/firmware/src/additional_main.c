#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
#include <string.h>

volatile uint8_t timer_callback_flag = 0;

void timer_callback( uintptr_t context ){
    timer_callback_flag = 1;
}

int secure_main( void ){
    

    /******************* Nastaveni seriove komunikace ******************/
    USART_SERIAL_SETUP my_sercom2_usart_setting;

    my_sercom2_usart_setting.baudRate = 115200;
    my_sercom2_usart_setting.dataWidth = USART_DATA_8_BIT;
    my_sercom2_usart_setting.parity = USART_PARITY_NONE;
    my_sercom2_usart_setting.stopBits = USART_STOP_1_BIT;
    
    SERCOM2_USART_SerialSetup(&my_sercom2_usart_setting, 0);
    /*******************************************************************/
    
    SYS_TIME_CallbackRegisterMS(timer_callback, 0, 500, SYS_TIME_PERIODIC);
    
    while ( true )
    {
        if(timer_callback_flag){
            
            printf("Timer byl vyvolan\r\n");
            LED_Toggle();
            
            timer_callback_flag = 0;
        }
    }
}