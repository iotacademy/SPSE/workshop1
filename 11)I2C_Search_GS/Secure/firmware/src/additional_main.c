#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes

void delay_ms(uint32_t ms){
    SYSTICK_TimerStart();
    SYSTICK_DelayMs(ms);
    SYSTICK_TimerStop();
}


uint16_t i2c_slave_adress = 0;
uint8_t i2c_reg_adress = 0;

uint8_t i2c_read_buff[127] = {}; 


int secure_main( void ) {
    
    printf("---> I2C search peripheral pulled example <---\r\n");
    
    while(1){
        SERCOM0_I2C_WriteRead(i2c_slave_adress, &i2c_reg_adress, 1, i2c_read_buff, 1);
        
        while(SERCOM0_I2C_IsBusy() == 1);
        
        if(SERCOM0_I2C_ErrorGet() == SERCOM_I2C_ERROR_NONE){
            printf("Slave address located on board is: 0x%02X\r\n", i2c_slave_adress);
        }

        if(i2c_slave_adress >= 127){
            printf("\r\n");

            delay_ms(2000);

            i2c_slave_adress = 0;
        }
        else i2c_slave_adress++;

    }
    
    return (EXIT_SUCCESS);
}

