#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
#include <string.h>

#define ABP 0  // Activation by personalizition (muze byt potvrzovana a nebo nepotvrzovana -> musi se modulu zadat vsechny klice aby se mohl pripojit na loragateway)
#define OTAA 1 // Over the air activation (nepotrebuje vsechny klice aby se mohla pripojit k gateway)

//Nastav tvuj mode pro muratu
#define LORA_MODE ABP


#ifdef LORA_MODE
    #if(LORA_MODE == ABP)
        char *pole_prikazu[] = {
            "AT\r",              // Overeni komunikace
            "AT+MODE=0\r",       // Nastaeni ABP modu
            "AT+MODE?\r",        // Zjisteni zda byl mod nastaven
            "AT+VER?\r",        // Zjisteni verze FW
            "AT+UTX 1\rA\r",    // Odeslani dat nepotvrzene (UTX) -> potvrzene (CTX)
            0
        };
    #endif

    #if(LORA_MODE == OTAA)
        char *pole_prikazu[] = {
            "AT\r",               // Overeni komunikace
            "AT+MODE=1\r",        // Nastaveni OTAA modu
            "AT+MODE?\r",         // Zjisteni zda byl mod nastaven
            "AT+JOIN\r",          // Pripojeni do site
            0
        };
    #endif
#endif

uint8_t pole_prikazu_counter = 0;


char prijmuty_znak = '\0';
char vychytany_retezec[64] = {};
uint8_t vychytany_retezec_couter = 0;


uint8_t usart_modul_RX_flag = 0; // Vlajka pokud doslo ke spravnemu prijmuti dat skrze USART


// Callback pro chytani prijmutyh dat z lora modulu (kdyz se zavola tento callback tak se 100% ulozi do prijmuty_znak hodnota primuteho znaku)
void my_sercom1_usart_read_callback( uintptr_t context ){
    usart_modul_RX_flag = 1;

    vychytany_retezec[vychytany_retezec_couter++] = prijmuty_znak; // Zapise do (vychytavaciho) retezce predchozi precteny znak
    
    SERCOM1_USART_Read((void *)&prijmuty_znak, 1);
}


int secure_main( void ){
    
    SYSTICK_TimerStart(); // Zapnuti casovace pro funkci delay

    
    /********* Nastaveni seriove komunikace RX and TX callback *********/
    //SERCOM1_USART_WriteCallbackRegister(my_sercom1_usart_write_callback, 0);
    SERCOM1_USART_ReadCallbackRegister(my_sercom1_usart_read_callback, 0);
    /*******************************************************************/
    
    
    /******************* Nastaveni seriove komunikace ******************/
    USART_SERIAL_SETUP my_sercom1_usart_setting;

    my_sercom1_usart_setting.baudRate = 19200;
    my_sercom1_usart_setting.dataWidth = USART_DATA_8_BIT;
    my_sercom1_usart_setting.parity = USART_PARITY_NONE;
    my_sercom1_usart_setting.stopBits = USART_STOP_1_BIT;
    
    SERCOM1_USART_SerialSetup(&my_sercom1_usart_setting, 0);
    /*******************************************************************/
    
    
    printf("USART modul basic\r\n");
    
    
    SERCOM1_USART_Read((void *)&prijmuty_znak, 1); // zahajeni cteni (periferie bude cekat az prijde 1 znak a pak zavola callback)
    
    while ( true )
    {
        if(pole_prikazu[pole_prikazu_counter] != 0){
            
            SERCOM1_USART_Write((void *)pole_prikazu[pole_prikazu_counter], strlen(pole_prikazu[pole_prikazu_counter])); // Odeslani comandu do lora modulu
            pole_prikazu_counter++;
        }
        else pole_prikazu_counter = 0;
        
        
        SYSTICK_DelayMs(1000); // Casove spozdeni 1s
        
        
        /****************** Vypsani nashromazdeneho textu ******************/
        printf("%s", vychytany_retezec);
        if(vychytany_retezec_couter > 0) vychytany_retezec_couter = 0;
        /******************************************************************/
        
        for(uint8_t i = 0; i < sizeof(vychytany_retezec); i++) vychytany_retezec[i] = 0; // Promazani retezce
        
        SYSTICK_DelayMs(1000); // Casove spozdeni 1s
    }
}