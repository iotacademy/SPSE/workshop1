#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes

uint8_t timer_callback_flag = 0;

void timer_callback(uintptr_t context){
    timer_callback_flag = 1;
}

int main ( void )
{
    /* Initialize all modules */
    SYS_Initialize ( NULL );
    
    /******************* Nastaveni seriove komunikace ******************/
    USART_SERIAL_SETUP my_sercom2_usart_setting;
    
    my_sercom2_usart_setting.baudRate = 115200;
    my_sercom2_usart_setting.dataWidth = USART_DATA_8_BIT;
    my_sercom2_usart_setting.parity = USART_PARITY_NONE;
    my_sercom2_usart_setting.stopBits = USART_STOP_1_BIT;
    
    SERCOM2_USART_SerialSetup(&my_sercom2_usart_setting, 1000000);
    /*******************************************************************/
    
    SYS_TIME_CallbackRegisterMS(timer_callback, 0, 5000, SYS_TIME_PERIODIC);
    
    while ( true )
    {
        /* Maintain state machines of all polled MPLAB Harmony modules. */
        SYS_Tasks ( );
        
        if(timer_callback_flag){
            //LED_Toggle();
            printf("----------------------------------\r\n");
            printf("\r\nProbuzeni :-)\r\n\r\n\r\n");
            timer_callback_flag = 0;
        }
        
        for(uint8_t i = 0; i < 4; i++){
            LED_Toggle();
            printf("Neco provadim a potom hned usnu ;-)\r\n\r\n");
            
            SYSTICK_TimerStart();
            SYSTICK_DelayMs(500);
            SYSTICK_TimerStop();
        }
        
        printf("Usinam :-(\r\n");
        
        
        
        PM_StandbyModeEnter();
  
    }

    /* Execution should not come here during normal operation */

    return ( EXIT_FAILURE );
}


/*******************************************************************************
 End of File
*/

