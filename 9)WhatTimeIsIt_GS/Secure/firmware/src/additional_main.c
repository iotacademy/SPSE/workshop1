#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
#include <string.h>

char retezec_cas_nahravani[] = __TIME__;
char retezec_datum_nahravani[] = __DATE__;

char *pole_retezcu_mesicu[12] = {
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
};

uint8_t retezec_mesic_first_space = 0;
char retezec_mesic_nahravani[4] = {};


volatile uint8_t my_alarm_flag = 0;

void RTC_Callback(RTC_CLOCK_INT_MASK int_cause , uintptr_t  context)
{
    if (int_cause & RTC_CLOCK_INT_MASK_ALARM0)
    {
        my_alarm_flag = 1;
    }
}

char *DayOfWeek[7] = {
    "Nedele",
    "Pondeli",
    "Utery",
    "Streda",
    "Ctvrtek",
    "Patek",
    "Sobota"
};


int getWeekDay(int day, int month, int year){
    return (((day += month < 3) ? year-- : year - 2, 23*month/9 + day + 4 + year/4- year/100 + year/400)%7);
}

char *getStringByWeekDay(int weekday){
    return (DayOfWeek[weekday]);
}

int secure_main( void ){
    
    SYSTICK_TimerStart(); // Zapnuti casovace pro funkci delay

    /******************* Nastaveni seriove komunikace ******************/
    USART_SERIAL_SETUP my_sercom2_usart_setting;
    
    my_sercom2_usart_setting.baudRate = 115200;
    my_sercom2_usart_setting.dataWidth = USART_DATA_8_BIT;
    my_sercom2_usart_setting.parity = USART_PARITY_NONE;
    my_sercom2_usart_setting.stopBits = USART_STOP_1_BIT;
    
    SERCOM2_USART_SerialSetup(&my_sercom2_usart_setting, 32000000);
    /*******************************************************************/
    
    
    
    
    struct tm sys_time;   // Vytvoreni struktury (objektu) pro cas programu
    struct tm alarm_time; // Vytvoreni struktury (objektu) pro cas kdy se ma neco stat

    
    RTC_RTCCCallbackRegister(RTC_Callback, 0); // Zaregistrovaní callbacku, ktery se zavola kdyz se bude podminka kalendare shodovat


    // Nastavení systemoveho casu
    /* Set Time and date
      15-01-2018 12:00:00 Monday */
    sys_time.tm_hour = ((retezec_cas_nahravani[0] - 48)*10) + (retezec_cas_nahravani[1] - 48);      /* hour [0,23] */
    sys_time.tm_sec  = ((retezec_cas_nahravani[6] - 48)*10) + (retezec_cas_nahravani[7] - 48);      /* seconds [0,61] */
    sys_time.tm_min  = ((retezec_cas_nahravani[3] - 48)*10) + (retezec_cas_nahravani[4] - 48);      /* minutes [0,59] */
    
    printf("Kontrola casu a datumu: %s\r\n", retezec_datum_nahravani);
    
    
    for(uint8_t p = 0; p < 3; p++){
        switch(p){
            case 0:
                
                for(retezec_mesic_first_space = 0; (retezec_datum_nahravani[retezec_mesic_first_space] != ' ') && (retezec_mesic_first_space < 11); retezec_mesic_first_space++);
                
                printf("Nalezena mezera na pozici: %d\r\n", retezec_mesic_first_space);
                
                
                
                for(uint8_t t = 0; t < retezec_mesic_first_space; t++){  
                    retezec_mesic_nahravani[t] = retezec_datum_nahravani[t];
                }
                retezec_mesic_nahravani[3] = '\0';
                
                
                printf("Text je: %s\r\n", retezec_mesic_nahravani);
                
                for(uint8_t k = 0; k < 12; k++){
                    if(strncmp(pole_retezcu_mesicu[k], retezec_mesic_nahravani, 3) == 0) sys_time.tm_mon = k; /* month of year [0,11] */
                }
                
                printf("Mesic je: %d\r\n", sys_time.tm_mon);
                
            break;
            
            case 1:
                sys_time.tm_mday = ((retezec_datum_nahravani[retezec_mesic_first_space + 1] != ' ') ? ((retezec_datum_nahravani[retezec_mesic_first_space + 1] - 48) * 10) : 0) + (retezec_datum_nahravani[retezec_mesic_first_space + 2] - 48); /* day of month [1,31] */
                printf("Den mesice je: %d\r\n", sys_time.tm_mday);
            break;
            
            case 2:
                sys_time.tm_year = (((retezec_datum_nahravani[retezec_mesic_first_space + 4] - 48) * 1000) + ((retezec_datum_nahravani[retezec_mesic_first_space + 5] - 48) * 100) + ((retezec_datum_nahravani[retezec_mesic_first_space + 6] - 48) * 10) + (retezec_datum_nahravani[retezec_mesic_first_space + 7] - 48)) - 1900;      /* years since 1900 */
                printf("Rok je: %d\r\n", sys_time.tm_year);
            break;
        }
    }
           
       
    sys_time.tm_wday = getWeekDay(sys_time.tm_mday, sys_time.tm_mon, sys_time.tm_year + 1900); /* day of week [0,6] (Sunday = 0) */
                                /* tm_yday - day of year [0,365] */
                                /* tm_isdst - daylight savings flag */

    RTC_RTCCTimeSet(&sys_time); // Aplikovani (nastavovani) ulozeneho casu ve strukture

    
    // Nastaveni casu pro vykonani operace (12:00:03 -> tri vteriny po zapnuti se vyvola callback a kazdou dalsi minutu ve stejnem poctu vterin)
    /* Set Alarm Time and date. Generate alarm every day when Hour, Minute and Seconds match.
       15-01-2018 12:00:20 Monday */
    alarm_time.tm_hour = sys_time.tm_hour;
    alarm_time.tm_sec = sys_time.tm_sec + 3;
    alarm_time.tm_min = sys_time.tm_min;
    alarm_time.tm_mon = sys_time.tm_mon;
    alarm_time.tm_year = sys_time.tm_year;
    alarm_time.tm_mday = sys_time.tm_mday;
    alarm_time.tm_wday = sys_time.tm_wday;

    RTC_RTCCAlarmSet(&alarm_time, RTC_ALARM_MASK_SS); // Kazdou minutu v tu samou vterinu se vyvola callback
    
    printf("Posledni cas nahrani programu je: %02d:%02d:%02d. A stalo se tak: %02d.%02d.%04d.\r\n", sys_time.tm_hour, sys_time.tm_min, sys_time.tm_sec, sys_time.tm_mday, (sys_time.tm_mon + 1), (sys_time.tm_year + 1900));
    //printf("Posledni cas nahrani programu je: %s. A stalo se tak: %s.\r\n", __TIME__, __DATE__);

    while ( true )
    {
        if(my_alarm_flag == 1) // Pokud byl callbacj zavolan znamena to ze chceme neco vykonat a to udelame prave tady
        {
            struct tm now_time;
            
            RTC_RTCCTimeGet(&now_time);
            
            printf("Prave je %02d:%02d:%02d. Datum je dneska: %02d.%02d.%04d\r\n", now_time.tm_hour, now_time.tm_min, now_time.tm_sec, now_time.tm_mday, (now_time.tm_mon + 1), (now_time.tm_year + 1900));
            
            my_alarm_flag = 0;
        }
        
        SYSTICK_DelayMs(100);
        LED_Toggle();
    }
}