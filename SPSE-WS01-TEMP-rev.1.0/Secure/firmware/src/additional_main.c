#include <stddef.h>      // Defines NULL
#include <stdbool.h>     // Defines true
#include <stdlib.h>      // Defines EXIT_FAILURE
#include "definitions.h" // SYS function prototypes

#include "config/default/my_drivers/shtc3/shtc3.h" // Include shtc3 sensor driver
#include "i2c_functions.h"                         // Include i2c functions

#include "config/default/my_drivers/string_common/string_common.h"               // Include string_common library for easier work
#include "config/default/my_drivers/preprocesor_datetime/preprocesor_datetime.h" // Include preprocesor_datetime for get last time of compilation

#include "lora_config.h" // Include LoRa keys


#define MCU_FREC 1000000 // Overclocked frekvenci of MCU (Do not touch -> If you want to change it, you must change it also in Harmony v3 configurator)



#define MEASURE_DELAY 300  // Measure delay in seconds [1 - 3600]



/**************************************** Global variables ******************************************/
preprocesor_datetime_t preprocesor_time; // Create structure for preprocesor date & time

struct tm sys_time;   // Create structure for system time
struct tm alarm_time; // Create structure for alarm time


uint8_t status_byte = 0; // Variable for status byte (1B posted to the LoRa server)


char recv_char              = '\0'; // Received char
char recv_buffer[64]        = {};   // Received buffer
uint8_t recv_buffer_counter = 0;    // Received buffer counter

uint8_t murata_connect_comands_counter = 0; // Counter for connect command sending to the module


volatile uint8_t alarm_flag = 0; // Flag for RTC alarm callback


volatile uint8_t button_flag  = 0; // Status of button -> Callback has been called
volatile uint8_t button_state = 0; // State of button (ON / OFF)
/****************************************************************************************************/



/***************************** Structure and variable for program state *****************************/
typedef enum{
    PROGRAM_INIT           = 0,
    PROGRAM_READY          = 1,
    PROGRAM_RUNNING        = 2,
    PROGRAM_GOING_TO_SLEEP = 3,
    PROGRAM_SLEEP          = 4,
    PROGRAM_WAKEUPPING     = 5
}STATE_MACHINE_MAIN_e;

STATE_MACHINE_MAIN_e program_state = PROGRAM_INIT;
/****************************************************************************************************/



/************************************* M2 module functions ******************************************/
// RX callback -> If data arrive then the RX callback will be called  
void module_rx_callback( uintptr_t context ){
    recv_buffer[recv_buffer_counter++] = recv_char;
    SERCOM1_USART_Read((void *)&recv_char, 1);
}

// Module connect commands (Depends on lora_config.h file)
char *murata_connect_comands[] = {
    "AT+MODE=0\r",      // ABP
    "AT+PORT=2\r",      // Port 2
    "AT+DFORMAT=1\r",   // HEX data
    "AT+DUTYCYCLE=0\r", // Disabled duty cycle
    "AT+DEVADDR="CONF_LORA_ABP_DEVADDR"\r",
    "AT+DEVEUI="CONF_LORA_ABP_DEVEUI"\r",
    "AT+APPSKEY="CONF_LORA_ABP_APPSKEY"\r",
    "AT+NWKSKEY="CONF_LORA_ABP_NWKSKEY"\r",
    "AT+ADR=1\r",       // Adaptive data rate enabled
    NULL
};
/****************************************************************************************************/



/***************************************** SHTC3 sensor *********************************************/
shtc3_t shtc3; // Create SHTC3 sensor structure (It holds informations about the sensor)
/****************************************************************************************************/



/************************************** RTC Callback ************************************************/
void RTC_Callback(RTC_CLOCK_INT_MASK int_cause , uintptr_t  context){
    
    if (int_cause & RTC_CLOCK_INT_MASK_ALARM0){
        
        alarm_flag = 1; // If RTC callback happend -> append alarm_flag variable with value 1
    }
}
/****************************************************************************************************/



/************************************** Button Callback *********************************************/
void button_callback(uintptr_t context){
    
    button_flag = 1; // If button callback happend -> append button_flag variable with value 1
    
    switch(button_state){
        case 0:
            USER_LED_Set();
            button_state = 1;
        break;
        
        case 1:
            USER_LED_Clear();
            button_state = 0;
        break;
        
        default:
            button_state = 0;
        break;
    }
}
/****************************************************************************************************/



/*********************************** Prototypes of functions ****************************************/
void module_send_data(uint8_t stat, shtc3_data_t *data); // Prototype of module_send_data function
void module_read_response( void ); // Prototype of module_read_response function

void convert_ptime_to_tmtime(preprocesor_datetime_t *ptime, struct tm *tmtime); // Prototype of convert_ptime_to_tmtime function

void delay_ms(uint32_t ms); // Prototype of delay_ms function
/****************************************************************************************************/



int secure_main( void ){
    
    
    /******************************** DEBUG USART setting ************************************/
    USART_SERIAL_SETUP sercom2_usart_settings;

    sercom2_usart_settings.baudRate  = 115200;
    sercom2_usart_settings.dataWidth = USART_DATA_8_BIT;
    sercom2_usart_settings.parity    = USART_PARITY_NONE;
    sercom2_usart_settings.stopBits  = USART_STOP_1_BIT;
    
    SERCOM2_USART_SerialSetup(&sercom2_usart_settings, MCU_FREC);
    /*****************************************************************************************/
    
    
    
    /******************************** MODULE USART setting ***********************************/
    SERCOM1_USART_ReadCallbackRegister(module_rx_callback, 0);
    
    USART_SERIAL_SETUP sercom1_usart_settings;

    sercom1_usart_settings.baudRate  = 19200;
    sercom1_usart_settings.dataWidth = USART_DATA_8_BIT;
    sercom1_usart_settings.parity    = USART_PARITY_NONE;
    sercom1_usart_settings.stopBits  = USART_STOP_1_BIT;
    
    SERCOM1_USART_SerialSetup(&sercom1_usart_settings, MCU_FREC);
    /****************************************************************************************/
    
    
    
    EIC_CallbackRegister(EIC_PIN_1, button_callback, 0); // Register callback for button
    
    
    
    shtc3_init(&shtc3, SHTC3_SLAVE_ADDRESS_DEFAULT, delay_ms, user_i2c_write, user_i2c_read_only); // Initialize shtc3 sensor
    
    
    
    getPreprocesorDateTime(&preprocesor_time); // Get preprocesor date & time
    
    convert_ptime_to_tmtime(&preprocesor_time, &sys_time); // Convert preprocesor date & time to tm date & time
    
    RTC_RTCCCallbackRegister(RTC_Callback, (uintptr_t)&alarm_time);
    
    RTC_RTCCTimeSet(&sys_time); // Setup RTC time
    
    alarm_time.tm_hour = sys_time.tm_hour;
    alarm_time.tm_sec = sys_time.tm_sec + 5;
    alarm_time.tm_min = sys_time.tm_min;
    alarm_time.tm_mon = sys_time.tm_mon;
    alarm_time.tm_year = sys_time.tm_year;
    alarm_time.tm_mday = sys_time.tm_mday;
    alarm_time.tm_wday = sys_time.tm_wday;

    RTC_RTCCAlarmSet(&alarm_time, RTC_ALARM_MASK_YYMMDDHHMMSS); // If second equals -> callback
    
    
    
    printf(" _____ _____ _____ _____           _ _ _ _____ ___ ___             _____ _____ _____ _____ \r\n");
    printf("|   __|  _  |   __|   __|   ___   | | | |   __|   |_  |     ___   |_   _|   __|     |  _  |\r\n");
    printf("|__   |   __|__   |   __|  |___|  | | | |__   | | |_| |_   |___|    | | |   __| | | |   __|\r\n");
    printf("|_____|__|  |_____|_____|         |_____|_____|___|_____|           |_| |_____|_|_|_|__|   \r\n");
    printf("\r\n");   
    
    
    
    printf(" _____                         _    _          _____ ____      _       \r\n");
    printf("|  _  |___ _ _ _ ___ ___ ___ _| |  | |_ _ _   |_   _|    \\ ___| |_ ___ \r\n");
    printf("|   __| . | | | | -_|  _| -_| . |  | . | | |    | | |  |  | .'|  _| .'|\r\n");
    printf("|__|  |___|_____|___|_| |___|___|  |___|_  |    |_| |____/|__,|_| |__,|\r\n");
    printf("                                       |___|                           \r\n");
    printf("\r\n");
    printf("\r\n");
    
    
    
    printf("-> LAST FW UPDATE %02d.%02d. %04d %02d:%02d:%02d\r\n", preprocesor_time.month_day, preprocesor_time.month + 1, preprocesor_time.year + 1900, preprocesor_time.hour, preprocesor_time.minute, preprocesor_time.second);
    
    
    
    printf("-> Initialization, one moment please :-)\r\n\r\n");
    
    
    while(1){
    
        switch(program_state){
        
            case PROGRAM_INIT:
 
                if(murata_connect_comands[murata_connect_comands_counter] != NULL){
                    
                    printf("Command posted to module: %s\n", murata_connect_comands[murata_connect_comands_counter]);
                    
                    SERCOM1_USART_Write(murata_connect_comands[murata_connect_comands_counter], strlen(murata_connect_comands[murata_connect_comands_counter]));
                    murata_connect_comands_counter++;
                    
                    delay_ms(200);
                }
                else{
                    
                    murata_connect_comands_counter = 0;
                    
                    SERCOM1_USART_Read((void *)&recv_char, 1);
                    
                    printf("\r\n");
                    
                    delay_ms(500);
                    
                    program_state = PROGRAM_READY;
                }

            break;
            
            case PROGRAM_READY:

                printf("-> Initialization OK\r\n");

                program_state = PROGRAM_RUNNING;
                
            break;
            
            case PROGRAM_RUNNING:
                
                if(alarm_flag | button_flag){
                    
                    if(alarm_flag){
                        
                        RTC_RTCCTimeGet(&alarm_time);
        
                        #if(MEASURE_DELAY <= 59)

                            if((alarm_time.tm_sec + MEASURE_DELAY) >= 60){
                                alarm_time.tm_sec = alarm_time.tm_sec + MEASURE_DELAY - 59;
                            }
                            else{
                                alarm_time.tm_sec = alarm_time.tm_sec + MEASURE_DELAY;
                            }
                        
                            RTC_RTCCAlarmSet(&alarm_time, RTC_ALARM_MASK_SS);

                        #endif

                        #if(MEASURE_DELAY >= 60)

                            if((alarm_time.tm_sec + (MEASURE_DELAY % 60)) >= 60){
                                alarm_time.tm_sec = alarm_time.tm_sec + (MEASURE_DELAY % 60) - 59;
                                alarm_time.tm_min++;
                            }
                            else{
                                alarm_time.tm_sec = alarm_time.tm_sec + (MEASURE_DELAY % 60);
                            }

                            if((alarm_time.tm_min + (MEASURE_DELAY / 60)) >= 60){
                                alarm_time.tm_min = alarm_time.tm_min + (MEASURE_DELAY / 60) - 59;
                            }
                            else{
                                alarm_time.tm_min = alarm_time.tm_min + (MEASURE_DELAY / 60);
                            }
                            
                            RTC_RTCCAlarmSet(&alarm_time, RTC_ALARM_MASK_MMSS);
                        
                        #endif

                    }
                    
                    RTC_RTCCTimeGet(&sys_time);
                    
                    printf("\r\n-> System time %02d.%02d. %04d %02d:%02d:%02d\r\n", sys_time.tm_mday, sys_time.tm_mon + 1, sys_time.tm_year + 1900, sys_time.tm_hour, sys_time.tm_min, sys_time.tm_sec);
                    
                    
                    shtc3_read_data(&shtc3); // Read data from sensor

                    printf("\r\nTemperature: %02d.%02d\r\nHumidity: %02d.%02d\r\nButton state: %d\r\n", (shtc3.data.temperature_integer - 45), shtc3.data.temperature_decimal, shtc3.data.humidity_integer, shtc3.data.humidity_decimal, button_state);

                    
                    status_byte = ((button_state) ? (status_byte | 0b00000001) : (status_byte & 0b11111110)); // write button state to last bit with the lowest grade

                    
                    module_send_data(status_byte, &shtc3.data); // Send data to lora server


                    delay_ms(100); // Wait for incoming data


                    module_read_response(); // Read module response
                    
                    
                    program_state = PROGRAM_GOING_TO_SLEEP;
                    
                    button_flag = 0; // Clear button flag
                    alarm_flag  = 0; // Clear alarm flag
                }
                
            break;
            
            case PROGRAM_GOING_TO_SLEEP:
                
                // Shut down USART bus
                
                printf("-> Going to sleep :-(\r\n");
                
                // Module USART vypnuti komunikace
                SERCOM1_USART_ReceiverDisable();
                SERCOM1_USART_TransmitterDisable();
                
                // DEBUG USART vypnuti komunikace
                SERCOM2_USART_ReceiverDisable();
                SERCOM2_USART_TransmitterDisable();
                
                program_state = PROGRAM_SLEEP;
                
            break;
            
            case PROGRAM_SLEEP:
                
                // Go sleep
                
                PM_StandbyModeEnter(); // Enter standby sleep mode (Program stops here and wait for interaction -> RTC or Button[EIC - External Interrupt Controller])
                
                program_state = PROGRAM_WAKEUPPING;
                
            break;
            
            case PROGRAM_WAKEUPPING:
                
                // Turn ON USART communication

                SERCOM1_USART_ReceiverEnable();
                SERCOM1_USART_TransmitterEnable();
                
                SERCOM2_USART_ReceiverEnable();
                SERCOM2_USART_TransmitterEnable();
                
                printf("\r\n-> Board is awakened :-)\r\n");
                
                program_state = PROGRAM_READY;
                
            break;
        } 
    }

    return ( EXIT_FAILURE );
}


void delay_ms(uint32_t ms){
    SYSTICK_TimerStart();
    SYSTICK_DelayMs(ms);
    SYSTICK_TimerStop();
}


void module_read_response( void ){

    printf("Module says: %s", recv_buffer);
    
    
    /******************************** Controls if data were posted or not *********************************/
    if(strncmp(recv_buffer, (char *)"+ERR", 4) == 0){
        
        USER_LED_Toggle();
        
        if(button_state) button_state = 0;
        else button_state = 1;
        
        printf("Data were not been posted -> button_state = %d\r\n", button_state);
    }
    /******************************************************************************************************/
    
    
    if(recv_buffer_counter > 0) recv_buffer_counter = 0;
    
    for(uint8_t i = 0; i < sizeof(recv_buffer); i++) recv_buffer[i] = 0; 
}

void _module_send_raw_data(uint8_t *in_buff){
    
    // AT+UTX |n|\r|data|\r
    
    uint8_t _help_buff[32] = "AT+UTX 5\r";
    
    for(uint8_t i = 0; i < 10; i++){
        _help_buff[i + 9] = in_buff[i];
    }
    
    _help_buff[19] = '\r';
    _help_buff[20] = '\0';
    
    SERCOM1_USART_Write(_help_buff, 19); // Sending command to lora module
    
    while(SERCOM1_USART_WriteIsBusy() == 1); // Maybe useless (Waiting for transmission to lora module)
    
    printf("\r\nData posted to module: %s\r\n", _help_buff);
}


void module_send_data(uint8_t stat, shtc3_data_t *data){
    
    static uint8_t _help_buff[3] = {};
    static uint8_t _send_buff[32] = {};
    
    
    NumToString(_help_buff, stat, 16, 2);
    
    for(uint8_t i = 0; i < 2; i++){
        _send_buff[i + 0] = _help_buff[i];
    }
    
    
    if((data->temperature_integer > 170) && (data->temperature_integer < 0)){
        data->temperature_integer = 0;
        data->temperature_decimal = 0;
    }
    
    
    NumToString(_help_buff, data->temperature_integer, 16, 2);

    for(uint8_t i = 0; i < 2; i++){
        _send_buff[i + 2] = _help_buff[i];
    }

    NumToString(_help_buff, data->temperature_decimal, 16, 2);

    for(uint8_t i = 0; i < 2; i++){
        _send_buff[i + 4] = _help_buff[i];
    }
    
    
    
    if((data->humidity_integer > 100) && (data->temperature_integer < 0)){
        data->humidity_integer = 0;
        data->humidity_decimal = 0;
    }
    
    
    NumToString(_help_buff, data->humidity_integer, 16, 2);
    
    for(uint8_t i = 0; i < 2; i++){
        _send_buff[i + 6] = _help_buff[i];
    }
    
    NumToString(_help_buff, data->humidity_decimal, 16, 2);
    
    for(uint8_t i = 0; i < 2; i++){
        _send_buff[i + 8] = _help_buff[i];
    }
    
    _send_buff[10] = '\0';
    
    _module_send_raw_data(_send_buff);
}

void convert_ptime_to_tmtime(preprocesor_datetime_t *ptime, struct tm *tmtime){
    tmtime->tm_hour = (int)ptime->hour;
    tmtime->tm_min  = (int)ptime->minute;
    tmtime->tm_sec  = (int)ptime->second;
    tmtime->tm_mday = (int)ptime->month_day;
    tmtime->tm_mon  = (int)ptime->month;
    tmtime->tm_year = (int)ptime->year;
}



