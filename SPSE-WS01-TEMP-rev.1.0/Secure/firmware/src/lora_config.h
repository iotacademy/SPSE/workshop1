/*
  @File Name
    lora_config.h

  @Summary
    Configuration file for lora communication.
*/

/* LoRa ABP NwkSessKey 16B */
#define CONF_LORA_ABP_NWKSKEY "ee11938caaab9ba20d581dcd3802bf81"

/* LoRa ABP AppSKey 16B */
#define CONF_LORA_ABP_APPSKEY "4f09e2d9764120159a7dfd840a1c4736"

/* LoRa ABP DevAddr 4B */
#define CONF_LORA_ABP_DEVADDR "09397c33"

/* LoRa ABP DevEui 8B */
#define CONF_LORA_ABP_DEVEUI "240323273ca35c6a"