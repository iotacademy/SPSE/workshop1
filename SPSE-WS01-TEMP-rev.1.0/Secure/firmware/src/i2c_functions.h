/* 
 * File:   i2c_functions.h
 * Author: mirda
 *
 * Created on May 9, 2021, 10:03 AM
 */

#ifndef I2C_FUNCTIONS_H
#define	I2C_FUNCTIONS_H


/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */

#include <stdint.h>
#include "config/default/peripheral/sercom/i2c_master/plib_sercom0_i2c_master.h"


#ifdef	__cplusplus
extern "C" {
#endif
    

    uint8_t user_i2c_read_only(shtc3_t *me, uint8_t *data, uint8_t len){
    
        uint8_t _state = 0;                                             // If 1 correct post data, if 0 could not post data

        _state = SERCOM0_I2C_Read(me->slave_address, data, len);        // Send register and get data from that register through the I2C bus

        while(SERCOM0_I2C_IsBusy() == 1);                               // Wait for data transmission is done

        if(SERCOM0_I2C_ErrorGet() != SERCOM_I2C_ERROR_NONE) _state = 0; // If ERROR -> _state = 0

        return _state;                                                  // return state of the bus communication
    }

    uint8_t user_i2c_write(shtc3_t *me, uint8_t reg, uint8_t *data, uint8_t len){

        uint8_t _state = 0;                                                  // If 1 correct post data, if 0 could not post data

        uint8_t _i2c_buff[19] = {};                                          // Create an _i2c_buff

        _i2c_buff[0] = reg;                                                  // Set selected register to the first position of _i2c_buff

        for(uint8_t i = 1; i < (len + 1); i++) _i2c_buff[i] = data[i-1];     // Copy all data to an _i2c_buffer

        _state = SERCOM0_I2C_Write(me->slave_address, _i2c_buff, (len + 1)); // Send data through the I2C bus

        while(SERCOM0_I2C_IsBusy() == 1);                                    // Wait for data transmission is done

        if(SERCOM0_I2C_ErrorGet() != SERCOM_I2C_ERROR_NONE) _state = 0;      // If ERROR -> _state = 0

        return _state;                                                       // return state of the bus communication
    }


#ifdef	__cplusplus
}
#endif

#endif	/* I2C_FUNCTIONS_H */

