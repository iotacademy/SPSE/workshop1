/*
 * Copyright 2021 Miroslav Soukup

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *  http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

/*
  @File Name
    preprocesor_datetime.h

  @Summary
    Library for get the preprocesor string datetime to value.
*/

#ifndef _PREPROCESOR_DATETIME_H_INCLUDED
#define _PREPROCESOR_DATETIME_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

/********************************************************************/
/* INCLUDES: Include external header files							*/
/********************************************************************/

#include <stdint.h>
#include <string.h>    
    
    
    /* ************************************************************************** */
    /* Section: Constants                                                         */
    /* ************************************************************************** */
    
    #define PREPROCESOR_DATETIME_ERR __UINT8_C(0x00) // returned state of fuction ERROR
    #define PREPROCESOR_DATETIME_OK  __UINT8_C(0x01) // returned state of fuction OK
    

    
    // *****************************************************************************
    // Section: Data Types
    // *****************************************************************************
    
    typedef struct preprocesor_datetime_descriptor_t preprocesor_datetime_t;
    
    
    struct preprocesor_datetime_descriptor_t{
        uint8_t	second;    // Seconds
        uint8_t minute;    // Minutes
        uint8_t hour;      // Hours
        uint8_t	month_day; // Day of month
        uint8_t month;     // Month
        uint16_t year;     // Year
        uint8_t week_day;  // Day of week 
    };



    

    // *****************************************************************************
    // Section: Function Prototypes
    // *****************************************************************************

    uint8_t getPreprocesorDateTime(preprocesor_datetime_t *pdt);
    
    char *getStringByWeekDay(uint8_t weekday);
    
    uint8_t getWeekDay(uint8_t day, uint8_t month, uint16_t year);
    
    
#ifdef __cplusplus
}
#endif

#endif /* _PREPROCESOR_DATETIME_H_INCLUDED */