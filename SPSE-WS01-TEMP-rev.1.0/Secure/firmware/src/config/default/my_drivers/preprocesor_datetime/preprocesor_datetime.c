/*
 * Copyright 2021 Miroslav Soukup

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *  http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

/*
  @File Name
    preprocesor_datetime.c

  @Summary
    Library for get the preprocesor string datetime to value.
*/


/********************************************************************/
/* INCLUDES: Include external header files							*/
/********************************************************************/

#include <stdint.h>
#include <string.h> 

#include "preprocesor_datetime.h"


char string_program_time[] = __TIME__;
char string_program_date[] = __DATE__;

char *string_array_of_months[12] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

char *string_array_of_days_of_week[7] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};




uint8_t getWeekDay(uint8_t day, uint8_t month, uint16_t year){
    return (((day += month < 3) ? year-- : year - 2, 23*month/9 + day + 4 + year/4- year/100 + year/400)%7);
}

char *getStringByWeekDay(uint8_t weekday){
    return (string_array_of_days_of_week[weekday]);
}


uint8_t getPreprocesorDateTime(preprocesor_datetime_t *pdt){
    pdt->hour   = (uint8_t)(((string_program_time[0] != ' ') ? ((string_program_time[0] - 48)*10) : 0) + (string_program_time[1] - 48)); // hour [0,23]
    pdt->minute = (uint8_t)(((string_program_time[3] != ' ') ? ((string_program_time[3] - 48)*10) : 0) + (string_program_time[4] - 48)); // minutes [0,59]
    pdt->second = (uint8_t)(((string_program_time[6] != ' ') ? ((string_program_time[6] - 48)*10) : 0) + (string_program_time[7] - 48)); // seconds [0,59]
    
    uint8_t _counter;
    
    for(_counter = 0; (string_program_date[_counter] != ' ') && (_counter < 11); _counter++);


    for(uint8_t k = 0; k < 12; k++){
        if(strncmp(string_array_of_months[k], string_program_date, 3) == 0) pdt->month = (uint8_t)k; // month of year [0,11]
    }
    
    pdt->month_day = (uint8_t)((string_program_date[4] != ' ') ? ((string_program_date[4] - 48) * 10) : 0) + (string_program_date[5] - 48); // day of month [1,31]
    
    pdt->year = ((((string_program_date[7] - 48) * 1000) + ((string_program_date[8] - 48) * 100) + ((string_program_date[9] - 48) * 10) + (string_program_date[10] - 48)) - 1900); // years since 1900
    
    pdt->week_day = getWeekDay(pdt->month_day, pdt->month, pdt->year + 1900);
    
    return PREPROCESOR_DATETIME_OK;
}