/*
 * Copyright 2021 Miroslav Soukup

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *  http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

/*
  @File Name
    string_common.c

  @Summary
    Library for easier work with string.
*/


/********************************************************************/
/* INCLUDES: Include external header files							*/
/********************************************************************/

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h> 

#include "string_common.h"



uint8_t get_char_index_in_string(char *string, uint8_t one_char){
    return ((uint8_t)(strchr(string, one_char) - string));
}
    
char *slice_string(char *in_buff, uint8_t start_index, uint8_t end_index){
    static char _slice_string_out_buff[128];
    uint8_t _slice_string_out_buff_index_counter = 0;

    for(uint8_t i = start_index; i < end_index; i++){
        _slice_string_out_buff[_slice_string_out_buff_index_counter++] = in_buff[i];
    }
    _slice_string_out_buff[_slice_string_out_buff_index_counter] = '\0';
    return _slice_string_out_buff;
}

uint8_t get_one_char_number(char znak){
    return ((uint8_t)(znak - 48));
}

char convert_one_char_number_to_char(uint8_t number){
    return ((char)(number + 48));
}

uint8_t NumToString(uint8_t *buffer, uint32_t number, uint8_t const num_base, uint8_t const view_size){
    
    static uint8_t _number_by_order[32] = {0x00};
    uint8_t _temp_counter = 0;
    uint8_t _count = 0;
        
    do{
        _number_by_order[_temp_counter++] = number % num_base;
        number /= num_base;
    }while(number > 0);
    
    int _temp_view_size = view_size;
 
    for(uint8_t i = _temp_counter; i > 0;){
        if(_temp_view_size > _temp_counter){
            buffer[_count++] = 48; // '0'
            _temp_view_size--;
        }
        else{
            buffer[_count++] = (_number_by_order[i-1] >= 10)? _number_by_order[i-1] + 55 : _number_by_order[i-1] + 48;
            i--;
        }
    }
    
    buffer[_count] = 0; // '\0'
    
    return _count;
}