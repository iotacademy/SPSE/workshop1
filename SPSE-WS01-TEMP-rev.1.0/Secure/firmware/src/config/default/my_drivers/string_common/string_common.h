/*
 * Copyright 2021 Miroslav Soukup

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *  http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

/*
  @File Name
    string_common.h

  @Summary
    Library for easier work with string.
*/

#ifndef _STRING_COMMON_H_INCLUDED
#define _STRING_COMMON_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

/********************************************************************/
/* INCLUDES: Include external header files							*/
/********************************************************************/

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>    
    
    
    
uint8_t get_char_index_in_string(char *string, uint8_t one_char);  

char *slice_string(char *in_buff, uint8_t start_index, uint8_t end_index);    

uint8_t get_one_char_number(char znak);

char convert_one_char_number_to_char(uint8_t number);

uint8_t NumToString(uint8_t *buffer, uint32_t number, uint8_t const num_base, uint8_t const view_size);
    
#ifdef __cplusplus
}
#endif

#endif /* _STRING_COMMON_H_INCLUDED */