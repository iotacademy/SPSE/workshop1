/*
 * Copyright 2021 Miroslav Soukup

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *  http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

/*
  @File Name
    shtc3.c

  @Summary
    Driver for sensirion temperature & humidity sensor shtc3.
*/


/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */

#include "shtc3.h"

#include <stdint.h>



/* ************************************************************************** */
// Section: Interface Functions                                               */
/* ************************************************************************** */

/********************************************************* SHTC3 INIT FUNCTION ******************************************************************/
/*
   Description:
                Function for initializing shtc3 driver.

   Parameters:
                me [in/out]            -> Structure instance of shtc3_t
                slave_address [in]     -> Contains the slave address of I2C device
                delay_ms_funcptr [in]  -> Contains pointer to the function for delay
                i2c_write_funcptr [in] -> Contains pointer to the function for I2C write
                i2c_read_funcptr [in]  -> Contains pointer to the function for I2C read
   
   Returns:
                uint8_t intern state of function : one -> Success / zero -> Failure
*/
uint8_t shtc3_init(shtc3_t *me, uint16_t slave_address, shtc3_delay_ms_funcptr delay_ms_funcptr, shtc3_i2c_write_funcptr i2c_write_funcptr, shtc3_i2c_read_only_funcptr i2c_read_funcptr){
    
    uint8_t _state = SHTC3_ERR;      // If 1 correct post data, if 0 could not post data
    
    me->slave_address = slave_address; // Set slave address to the device
    
    me->delay_ms      = delay_ms_funcptr; // Set delay function
    
    me->i2c_write     = i2c_write_funcptr; // Set i2c write function
    me->i2c_read_only = i2c_read_funcptr;  // Set i2c read function

    _state = SHTC3_OK;               // Set state to 1 to indicate no error  
    
    return _state;                     // return state
}
/**************************************************************************************************************************************************/


// Function for change structure of the i2c function created in main
static uint8_t _shtc3_i2c_write_command(shtc3_t *me, uint8_t *command){
    return me->i2c_write(me, command[0], &command[1], 1);
}

// Function for wakeup sensor
static uint8_t _shtc3_write_wakeup(shtc3_t *me){ 
    uint8_t _buff[2] = {};
    
    _buff[0] = (SHTC3_WAKE_UP_COMMAND >> 8) & 0xFF; // Wake up (0x3517)
    _buff[1] = SHTC3_WAKE_UP_COMMAND & 0xFF;
    
    return _shtc3_i2c_write_command(me, _buff);
}

// Function for read humidity first configuration
static uint8_t _shtc3_write_RHReadDataFirst(shtc3_t *me){ 
    uint8_t _buff[2] = {};
    
    _buff[0] = (SHTC3_RH_FIRST_COMMAND >> 8) & 0xFF; // RH first read (0x5C24)
    _buff[1] = SHTC3_RH_FIRST_COMMAND & 0xFF;
                
    return _shtc3_i2c_write_command(me, _buff);
}


//Function for sensor sleep
static uint8_t _shtc3_write_sleep(shtc3_t *me){ 
    uint8_t _buff[2] = {};
    
    _buff[0] = (SHTC3_SLEEP_COMMAND >> 8) & 0xFF; // Go sleep (0xB098)
    _buff[1] = SHTC3_SLEEP_COMMAND & 0xFF;
                
    return _shtc3_i2c_write_command(me, _buff);
}



/************************************ SHTC3 READ DATA FUNCTION ***********************************/
/*
   Description:
                Read temperature & humidity from the sensor.

   Parameters:
                me [in/out] -> Structure instance of shtc3_t
   
   Returns:
                uint8_t intern state of function : one -> Success / zero -> Failure
*/
uint8_t shtc3_read_data(shtc3_t *me){
    
    uint8_t _data_buff[6] = {};

    uint32_t _help_hum = 0;
    uint32_t _help_temp = 0;
    
    _shtc3_write_wakeup(me);
                
    me->delay_ms(1); // Wait for starting up the shtc3 sensor
                
    _shtc3_write_RHReadDataFirst(me);
 
    me->delay_ms(13); // Wait for data conversion
             
    me->i2c_read_only(me, _data_buff, 6); // [HUM_MSB] [HUM_LSB] [HUM_CRC] [TEMP_MSB] [TEMP_LSB] [TEMP_CRC]
                
    _help_hum = ((((_data_buff[0] << 8) | _data_buff[1]) * 10000) / 65536);
                
    me->data.humidity_integer = _help_hum / 100;
    me->data.humidity_decimal = _help_hum - (me->data.humidity_integer * 100);
 
    _help_temp = ((((_data_buff[3] << 8) | _data_buff[4]) * 17500) / 65536); // Offset +45C
                
    me->data.temperature_integer = _help_temp / 100;
    me->data.temperature_decimal = _help_temp - (me->data.temperature_integer * 100);
                
    _shtc3_write_sleep(me);
    
    return SHTC3_OK;
}
/*************************************************************************************************/