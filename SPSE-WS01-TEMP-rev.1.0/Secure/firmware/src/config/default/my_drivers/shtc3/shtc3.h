/*
 * Copyright 2021 Miroslav Soukup

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 *  http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

/*
  @File Name
    shtc3.h

  @Summary
    Driver for sensirion temperature & humidity sensor shtc3.
*/


#ifndef _SHTC3_H    /* Guard against multiple inclusion */
#define _SHTC3_H


/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */

#include <stdint.h>


/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif

    
    /* ************************************************************************** */
    /* Section: Constants                                                         */
    /* ************************************************************************** */
    
    #define SHTC3_ERR __UINT8_C(0x00) // shtc3 returned state of fuction ERROR
    #define SHTC3_OK  __UINT8_C(0x01) // shtc3 returned state of fuction OK
    
    
    #define SHTC3_SLAVE_ADDRESS_DEFAULT __UINT16_C(0x70) // Default slave address of SHTC3 sensor

    #define SHTC3_WAKE_UP_COMMAND  __UINT16_C(0x3517) // Wake up command
    #define SHTC3_RH_FIRST_COMMAND __UINT16_C(0x5C24) // Humidity read first command
    #define SHTC3_SLEEP_COMMAND    __UINT16_C(0xB098) // Go sleep command



    // *****************************************************************************
    // Section: Data Types
    // *****************************************************************************

    typedef struct shtc3_data_descriptor_t shtc3_data_t;
    typedef struct shtc3_descriptor_t shtc3_t;

    typedef uint8_t (*shtc3_i2c_read_only_funcptr)(shtc3_t *me, uint8_t *data, uint8_t len);
    typedef uint8_t (*shtc3_i2c_write_funcptr)(shtc3_t *me, uint8_t reg, uint8_t *data, uint8_t len);
    typedef void (*shtc3_delay_ms_funcptr)(uint32_t ms);

    struct shtc3_data_descriptor_t{
        uint8_t temperature_integer;
        uint8_t temperature_decimal;

        uint8_t humidity_integer;
        uint8_t humidity_decimal;
    };

    struct shtc3_descriptor_t{
        uint16_t slave_address;

        shtc3_i2c_read_only_funcptr i2c_read_only;
        shtc3_i2c_write_funcptr i2c_write;
        shtc3_delay_ms_funcptr delay_ms;

        shtc3_data_t data;
    };


    

    // *****************************************************************************
    // Section: Function Prototypes
    // *****************************************************************************

    
    uint8_t shtc3_init(shtc3_t *me, uint16_t slave_address, shtc3_delay_ms_funcptr delay_ms_funcptr, shtc3_i2c_write_funcptr i2c_write_funcptr, shtc3_i2c_read_only_funcptr i2c_read_funcptr);

    uint8_t shtc3_read_data(shtc3_t *me);
    

    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _SHTC3_H */

/* *****************************************************************************
 End of File
 */
