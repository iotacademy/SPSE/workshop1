#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes

int secure_main( void ){
    
    SYSTICK_TimerStart(); // Zapnuti vnitrniho couteru pro funkci delay
    
    LED_Clear(); // Vypnuti LED1
    
    while ( true )
    {
        LED_Set();              // Zapnuti LED
        
        SYSTICK_DelayMs(1000); // Casove zpozdeni 1s
        
        LED_Toggle();          // Preklopeni zapnute LED do vypnuteho stavu
        
        SYSTICK_DelayMs(1000); // Casove zpozdeni 1s
    }
    
    return 0;
}