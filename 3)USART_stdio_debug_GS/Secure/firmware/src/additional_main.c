#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes


int secure_main( void ){
    
    SYSTICK_TimerStart(); // Zapnuti casovace pro funkci delay

    /******************* Nastaveni seriove komunikace ******************/
    USART_SERIAL_SETUP my_sercom2_usart_setting;

    my_sercom2_usart_setting.baudRate = 115200;
    my_sercom2_usart_setting.dataWidth = USART_DATA_8_BIT;
    my_sercom2_usart_setting.parity = USART_PARITY_NONE;
    my_sercom2_usart_setting.stopBits = USART_STOP_1_BIT;
    
    SERCOM2_USART_SerialSetup(&my_sercom2_usart_setting, 32000000);
    /*******************************************************************/
    
    while ( true )
    {
        printf("Test komunikace :-)\r\n");
        
        SYSTICK_DelayMs(1000); // Casove spozdeni 1s
    }
}