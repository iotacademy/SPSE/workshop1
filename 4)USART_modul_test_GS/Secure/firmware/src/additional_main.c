#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes
#include <string.h>

char test_retezec[] = "AT+MODE?\r";
char prijmuty_znak = '\0';
char vychytany_retezec[64] = {};
uint8_t vychytany_retezec_couter = 0;


uint8_t usart_modul_RX_flag = 0; // Vlajka pokud doslo ke spravnemu prijmuti dat skrze USART


// Callback pro chytani prijmutyh dat z lora modulu
void my_sercom1_usart_read_callback( uintptr_t context ){
    usart_modul_RX_flag = 1;
    
    LED_Toggle();
    
    vychytany_retezec[vychytany_retezec_couter++] = prijmuty_znak;
    
    SERCOM1_USART_Read((void *)&prijmuty_znak, 1);
}


int secure_main( void ){
    
    SYSTICK_TimerStart(); // Zapnuti casovace pro funkci delay

    /******************* Nastaveni seriove komunikace ******************/
    USART_SERIAL_SETUP my_sercom2_usart_setting;

    my_sercom2_usart_setting.baudRate = 115200;
    my_sercom2_usart_setting.dataWidth = USART_DATA_8_BIT;
    my_sercom2_usart_setting.parity = USART_PARITY_NONE;
    my_sercom2_usart_setting.stopBits = USART_STOP_1_BIT;
    
    SERCOM2_USART_SerialSetup(&my_sercom2_usart_setting, 0);
    /*******************************************************************/
    
    
    /********* Nastaveni seriove komunikace RX and TX callback *********/
    //SERCOM1_USART_WriteCallbackRegister(my_sercom1_usart_write_callback, 0);
    SERCOM1_USART_ReadCallbackRegister(my_sercom1_usart_read_callback, 0);
    /*******************************************************************/
    
    
    /******************* Nastaveni seriove komunikace ******************/
    USART_SERIAL_SETUP my_sercom1_usart_setting;

    my_sercom1_usart_setting.baudRate = 19200;
    my_sercom1_usart_setting.dataWidth = USART_DATA_8_BIT;
    my_sercom1_usart_setting.parity = USART_PARITY_NONE;
    my_sercom1_usart_setting.stopBits = USART_STOP_1_BIT;
    
    SERCOM1_USART_SerialSetup(&my_sercom1_usart_setting, 0);
    /*******************************************************************/
    
    
    
    
    SERCOM1_USART_Read((void *)&prijmuty_znak, 1); // zahajeni cteni (periferie bude cekat az prijde 1 znak a pak zavola callback)
    
    while ( true )
    {
        
        SERCOM1_USART_Write((void *)test_retezec, strlen(test_retezec)); // Odeslani comandu do lora modulu
        
        SYSTICK_DelayMs(1000); // Casove spozdeni 1s
        
        
        /****************** Vypsani nashromazdeneho textu ******************/
        for(uint8_t i = 0; i < vychytany_retezec_couter; i++){
            while(SERCOM2_USART_WriteIsBusy());
            SERCOM2_USART_Write((void *)&vychytany_retezec[i], 1);
        }
        if(vychytany_retezec_couter > 0) vychytany_retezec_couter = 0;
        /******************************************************************/
        
        
        SYSTICK_DelayMs(1000); // Casove spozdeni 1s
    }
}